function adicionarPartida(){
    const casa = document.getElementById("casa").value;
    const visitante = document.getElementById("visitante").value;

    const li = document.createElement("li");
    li.innerText = `${casa} X ${visitante} [Em andamento]`;

    document.getElementById("partidas").appendChild(li);

    let partida = new Promise(function(resolve, reject){
        setTimeout(() => {
            resolve();
        }, 4500);
    });

    partida.then(() =>{
        li.innerText = `${casa}: ${Math.ceil(Math.random() * 12)} X ${visitante}: ${Math.ceil(Math.random() * 10)} [Finalizada]`
    }).catch();
}
