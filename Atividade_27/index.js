const fs = require('fs');

const ARQUIVO = "palavras.json";

var existePalavra = null;
try{
    existePalavra = fs.readFileSync(ARQUIVO, "utf8");
}catch(err){}

if(existePalavra != null){
    existePalavra = JSON.parse(existePalavra);
}else{
    existePalavra = [];
}

const novaPalavra = process.argv.slice(2);

let palavrasNaoRepetidas = new Set([...existePalavra, ...novaPalavra]);
palavrasNaoRepetidas = [...palavrasNaoRepetidas].filter(p => p != null && p.trim().length > 0);

fs.writeFileSync(ARQUIVO, JSON.stringify(palavrasNaoRepetidas), { encoding: "utf8", flag: "w"});

