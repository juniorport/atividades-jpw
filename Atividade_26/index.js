const nums = process.argv.slice(2);

try{
    const somaDivisiveisDois = nums.map( n => parseInt(n)).filter(n => n%2 === 0).reduce((acc, n) => acc + n);
    console.log(somaDivisiveisDois);
}catch(err){
    console.err(err);
    console.log("Informe apenas números nos argumentos!");
}
