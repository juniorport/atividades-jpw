function adicionar(){

    let nome = document.getElementById("nome");
    let estilo = document.getElementById("estilo");
    let url = document.getElementById("url");
   
    let div = document.createElement("div");

    let h2 = document.createElement("h2");
    h2.innerText = "Nome: " + nome.value;
    div.appendChild(h2);

    let p = document.createElement("p");
    p.innerText = "Estilo: " + estilo.value;
    div.appendChild(p);

    let img = document.createElement("img");
    img.src = url.value;
    img.style.height = "300";
    div.appendChild(img);

    div.appendChild(document.createElement("hr"));

    document.body.appendChild(div);

    nome.value = "";
    estilo.value = "";
    url.value = "";
}
